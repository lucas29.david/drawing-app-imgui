#include "createWindow.hpp"

void createWindow(Window &window){
    const ImGuiViewport* viewport = ImGui::GetMainViewport();
    ImGui::SetNextWindowPos(window.use_work_area ? viewport->WorkPos : viewport->Pos);
    ImGui::SetNextWindowSize(window.use_work_area ? viewport->WorkSize : viewport->Size);
    
    window.p_open = NULL;
    ImGuiWindowFlags window_flags = 0;
    window_flags |= ImGuiWindowFlags_NoCollapse;

    ImGui::Begin("Test Window", window.p_open, window_flags);
}

void createMenu(Window &window){
    ImGui::Checkbox("Enable grid", &window.opt_enable_grid);
    ImGui::Text("Mouse Left: drag to add lines,\nMouse Right: drag to scroll, click for context menu.");
    ImGui::PushItemWidth(-ImGui::GetFontSize() * 15);
}

void createCanva(Window &window){
    window.draw_list = ImGui::GetWindowDrawList();
    ImGui::ColorEdit4("Color", &window.colf.x);
    window.p = ImGui::GetCursorScreenPos();
    window.col = ImColor(window.colf);
    window.x = window.p.x;
    window.y = window.p.y;

    window.canvas_p0 = ImGui::GetCursorScreenPos();
    window.canvas_sz = ImGui::GetContentRegionAvail();
    window.canvas_p1 = setCanva_p1(window.canvas_p0, window.canvas_sz);

    // Draw border and background color
    ImGuiIO& io = ImGui::GetIO();
    window.draw_list->AddRectFilled(window.canvas_p0, window.canvas_p1, IM_COL32(255, 255, 255, 255));
    window.draw_list->AddRect(window.canvas_p0, window.canvas_p1, IM_COL32(200, 200, 200, 255));
    

    // ImGui::Dummy(ImVec2((sz + spacing) * 10.2f, (sz + spacing) * 3.0f)); // Je ne comprends pas l'utilité de cette fonction ici
    ImGui::PopItemWidth();

    // This will catch our interactions
    ImGui::InvisibleButton("canvas", window.canvas_sz, ImGuiButtonFlags_MouseButtonLeft | ImGuiButtonFlags_MouseButtonRight);
    window.is_hovered = ImGui::IsItemHovered(); // Hovered
    window.is_active = ImGui::IsItemActive();   // Held
    window.origin = ImVec2(window.canvas_p0.x + window.scrolling.x, window.canvas_p0.y + window.scrolling.y); // Lock scrolled origin
    window.mouse_pos_in_canvas = ImVec2(io.MousePos.x - window.origin.x, io.MousePos.y - window.origin.y);


    // Add points

    const float mouse_threshold_for_pan = window.opt_enable_context_menu ? -1.0f : 0.0f;
    if (window.is_active && ImGui::IsMouseDragging(ImGuiMouseButton_Right, mouse_threshold_for_pan))
    {
        window.scrolling.x += io.MouseDelta.x;
        window.scrolling.y += io.MouseDelta.y;
    }

    // Context menu (under default mouse threshold)
    ImVec2 drag_delta = ImGui::GetMouseDragDelta(ImGuiMouseButton_Right);
    if (window.opt_enable_context_menu && ImGui::IsMouseReleased(ImGuiMouseButton_Right) && drag_delta.x == 0.0f && drag_delta.y == 0.0f)
        ImGui::OpenPopupOnItemClick("context");
}

ImVec2 setCanva_p1(ImVec2 &canvas_p0, ImVec2 &canvas_sz){
    if (canvas_sz.x < 50.0f) canvas_sz.x = 50.0f;
    if (canvas_sz.y < 50.0f) canvas_sz.y = 50.0f;
    ImVec2 canva_p1 = ImVec2(canvas_p0.x + canvas_sz.x, canvas_p0.y + canvas_sz.y);
    return canva_p1;
}