#include "shapes.hpp"
#include "ImGuiWrapper.h"

void shapeButton (Forme &forme) {
    
    if(ImGui::Button("carré")) forme = Forme::Square;
    if(ImGui::Button("ligne")) forme = Forme::Line; 
    if(ImGui::Button("crayon")) forme = Forme::Free;
}