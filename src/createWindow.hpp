#include "ImGuiWrapper.h"


struct Window
{
    bool use_work_area = true;

    bool* p_open;
    bool opt_enable_grid = true;
    bool opt_enable_context_menu = true;
    ImVec2 scrolling = ImVec2(0.0f, 0.0f);
    
    ImDrawList* draw_list;
    ImVec4 colf = ImVec4(1.0f, 0.0f, 0.0f, 1.0f);
    ImVec2 p;
    ImU32 col;
    float x;
    float y;

    ImVec2 canvas_p0;
    ImVec2 canvas_sz;
    ImVec2 canvas_p1;

    bool is_hovered;
    bool is_active;
    ImVec2 origin;
    ImVec2 mouse_pos_in_canvas;
};

ImVec2 setCanva_p1(ImVec2 &canvas_p0, ImVec2 &canvas_sz);
void createWindow(Window &window);
void createMenu(Window &window);
void createCanva(Window &window);