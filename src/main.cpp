#include "ImGuiWrapper.h"
#include <iostream>
#include "draw.hpp"

void draw_window(Shapes &shapes, Window &window)
{
    createWindow(window);
    shapeButton(shapes.forme);
    createMenu(window);
    createCanva(window);
    addElements(window, shapes);
    removeElements(shapes);
    drawElements(shapes, window);
    ImGui::End();
}

int main(int, char**)
{
    GLFWwindow* const window = ImGuiWrapper::create_window(1280, 720, "Simple ImGui Setup");
    Shapes shapes;
    Window myWindow;

    shapes.forme = Forme::Free;
    // Main loop
    while (!glfwWindowShouldClose(window)) {
        glfwPollEvents();

        ImGuiWrapper::start_frame();
        draw_window(shapes, myWindow);
        ImGuiWrapper::end_frame(window, {0.45f, 0.55f, 0.60f, 1.00f});
    }

    ImGuiWrapper::shutdown(window);
}
