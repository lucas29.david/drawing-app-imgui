#include "ImGuiWrapper.h"

class Curve {
    private:
        ImVector<ImVec2> _freePoints;            
        ImU32 _freeCol;
    public:
        Curve(const ImU32 col) : _freePoints(), _freeCol(col) {}
        ~Curve() {};

    void freePush_back(ImVec2 points){
        _freePoints.push_back(points);
    }

    void draw(ImDrawList* draw_list, const ImVec2 origin){
        if(_freePoints.Size>1){
            for (int n = 0; n < _freePoints.Size-1; n += 1){
                draw_list->AddLine(ImVec2(origin.x + _freePoints[n].x, origin.y + _freePoints[n].y), ImVec2(origin.x + _freePoints[n+1].x, origin.y + _freePoints[n+1].y), _freeCol, 2.0f);
            }
        }
    }
};
