#include "draw.hpp"

void addElements(Window &window, Shapes &shapes){
    switch (shapes.forme) {
        case Forme::Free: {
            if(window.is_active && ImGui::IsMouseClicked(ImGuiMouseButton_Left)){
                Curve curve(window.col);
                shapes.curves.push_back(curve);
                shapes.history.push_back(shapes.forme);
            }
            if(window.is_active && ImGui::IsMouseDown(ImGuiMouseButton_Left))
                shapes.curves.back().freePush_back(window.mouse_pos_in_canvas);
            break;
        }
        case Forme::Square: {
            ImVec2 firstCorner;
            if(window.is_active && ImGui::IsMouseClicked(ImGuiMouseButton_Left)){
                shapes.squareCol.push_back(window.col);
                firstCorner = window.mouse_pos_in_canvas;
                shapes.Corners.push_back(firstCorner);
                shapes.Corners.push_back(firstCorner);
                shapes.history.push_back(shapes.forme);
            }
            if(window.is_active && ImGui::IsMouseDown(ImGuiMouseButton_Left)){
                shapes.Corners[shapes.Corners.size()-1] = window.mouse_pos_in_canvas;
            }
            break;
        }
        case Forme::Line: {
            if (window.is_hovered && !shapes.adding_line && ImGui::IsMouseClicked(ImGuiMouseButton_Left))
            {
                shapes.lineCol.push_back(window.col);
                shapes.points.push_back(window.mouse_pos_in_canvas);
                shapes.points.push_back(window.mouse_pos_in_canvas);
                shapes.adding_line = true;
                shapes.history.push_back(shapes.forme);
            }
            if (shapes.adding_line)
            {
                shapes.points.back() = window.mouse_pos_in_canvas;
                if (!ImGui::IsMouseDown(ImGuiMouseButton_Left))
                    shapes.adding_line = false;
            }
            break;
        }
    }
}

void removeElements(Shapes &shapes){
    if (ImGui::BeginPopup("context"))
    {
        if (shapes.adding_line)
            shapes.points.resize(shapes.points.size() - 2);
        shapes.adding_line = false;
        if (ImGui::MenuItem("Remove one", NULL, false, shapes.history.Size > 0)) {
            switch (shapes.history.back()) {
                case Forme::Square:
                    shapes.Corners.resize(shapes.Corners.size() - 2);
                    break;
                case Forme::Line:
                    shapes.points.resize(shapes.points.size() - 2);
                    break;
                case Forme::Free:
                    shapes.curves.resize(shapes.curves.size() - 1);
                    break;
                
                default:
                    break;
            }
            shapes.history.resize(shapes.history.size() - 1);
        }
        if (ImGui::MenuItem("Remove all", NULL, false, shapes.history.Size > 0)) { shapes.points.clear(); shapes.Corners.clear(); shapes.curves.clear(); shapes.history.clear(); }
        ImGui::EndPopup();
    }
}

void drawElements(Shapes &shapes, Window &window){
    // Draw grid
    window.draw_list->PushClipRect(window.canvas_p0, window.canvas_p1, true);
    if (window.opt_enable_grid)
    {
        const float GRID_STEP = 64.0f;
        for (float x = fmodf(window.scrolling.x, GRID_STEP); x < window.canvas_sz.x; x += GRID_STEP)
            window.draw_list->AddLine(ImVec2(window.canvas_p0.x + x, window.canvas_p0.y), ImVec2(window.canvas_p0.x + x, window.canvas_p1.y), IM_COL32(200, 200, 200, 40));
        for (float y = fmodf(window.scrolling.y, GRID_STEP); y < window.canvas_sz.y; y += GRID_STEP)
            window.draw_list->AddLine(ImVec2(window.canvas_p0.x, window.canvas_p0.y + y), ImVec2(window.canvas_p1.x, window.canvas_p0.y + y), IM_COL32(200, 200, 200, 40));
    }
    // Draw lines
    for (int n = 0; n < shapes.points.Size; n += 2)
        window.draw_list->AddLine(ImVec2(window.origin.x + shapes.points[n].x, window.origin.y + shapes.points[n].y), ImVec2(window.origin.x + shapes.points[n + 1].x, window.origin.y + shapes.points[n + 1].y), shapes.lineCol[n/2], 2.0f);
    // Draw Free
    if (shapes.curves.Size>0){
        for (auto& curve : shapes.curves) curve.draw(window.draw_list, window.origin);
    }
    // Draw Squares
    for (int n = 0; n < shapes.Corners.size()-1; n+=2){
        window.draw_list->AddRect(ImVec2(shapes.Corners[n].x+window.x+window.scrolling.x, shapes.Corners[n].y+window.y+window.scrolling.y), ImVec2(shapes.Corners[n+1].x+window.x+window.scrolling.x, shapes.Corners[n+1].y+window.y+window.scrolling.y), shapes.squareCol[n/2], 0.0f, ImDrawFlags_None, 1.0f);
    }
    window.draw_list->PopClipRect();
}