#include "ImGuiWrapper.h"
#include "curve.hpp"
#include "shapes.hpp"
#include "math.h"
#include "createWindow.hpp"

struct Shapes
{
    bool adding_line = false;    
    ImVector<ImVec2> points;
    ImVector<ImU32> currentCol;
    ImVector<ImVec2> Corners;
    ImVector<ImU32> lineCol;
    ImVector<ImU32> squareCol;
    ImVector<Curve> curves;

    ImVector<Forme> history;

    Forme forme;
};

void addElements(Window &window, Shapes &shapes);
void removeElements(Shapes &shapes);
void drawElements(Shapes &shapes, Window &window);